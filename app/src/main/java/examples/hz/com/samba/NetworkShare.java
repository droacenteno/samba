package examples.hz.com.samba;

/**
 * Created by anton on 10-1-16.
 */
/**
 * @author Kushal Paudyal
 * Create on 2012-10-12
 * Last Modified On 2012-10-12
 * www.sanjaal.com/java, www.icodejava.com
 *
 * JCIFS is an Open Source client library that implements the CIFS/SMB networking protocol in 100% Java.
 * CIFS is the standard file sharing protocol on the Microsoft Windows platform
 * Visit their website at: jcifs.samba.org
 *
 * Uses: jcifs-1.1.11.jar
 *
 */
//example MPD: https://github.com/Ichimonji10/impedimenta/blob/master/Android/JeremysJamminJukebox/trunk/src/net/JeremyAudet/JeremysJamminJukebox
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.UnknownHostException;
import java.util.ArrayList;

import jcifs.smb.NtlmPasswordAuthentication;
import jcifs.smb.SmbException;
import jcifs.smb.SmbFile;
import jcifs.smb.SmbFileInputStream;
import org.bff.javampd.MPD;
import mpc.*;
import org.bff.javampd.exception.MPDConnectionException;

import jcifs.smb.SmbFileOutputStream;

public class NetworkShare {
    static final String USER_NAME = "wieneke";
    static final String PASSWORD = "wieneke";
    //e.g. Assuming your network folder is: \my.myserver.netsharedpublicphotos
    public static final int DEFAULT_PORT = 6600;
    public MPD mpd;
    static final String NETWORK_FOLDER = "smb://192.168.2.8/FamilyLibrary/favorites/m/Paul McCartney/1973 - Band On The Run/";

    public boolean doTest() {
        MusicDatabase db=new MusicDatabase() {
            @Override
            public void clear() {

            }

            @Override
            public void addSong(MPCSong song) {

            }

            @Override
            public void startTransaction() {

            }

            @Override
            public void setTransactionSuccessful() {

            }

            @Override
            public void endTransaction() {

            }
        };
        MPC mpc = new MPC("192.168.2.16", DEFAULT_PORT, 1000, db);
        ArrayList<Mp3File> list=mpc.playlist();
        //mpc.next();
        boolean successful = false;
        try{
            String user = USER_NAME + ":" + PASSWORD;
            Log.v("samba", "User: " + user);

            NtlmPasswordAuthentication auth = new NtlmPasswordAuthentication(user);

            SmbFile dir = new SmbFile(NETWORK_FOLDER, auth);
            for (SmbFile f : dir.listFiles()) {

                Log.v("samba", f.getName());
                f.isDirectory();
            }
            SmbFile smbFile = new SmbFile(NETWORK_FOLDER+"mp3info.txt", auth);
            ArrayList<String> builder;
            ArrayList<MPCSong> songs=new ArrayList<MPCSong>();
            try {
                builder = readFileContent(smbFile);

                for (String line:builder) {
                    Log.v("samba", line);
                }
                for (int i=1;i<builder.size();i++){
                    Mp3File mp=new Mp3File(builder.get(i));
                    Log.v("samba", mp.getArtist() + "-" + mp.getTracknr() + "-" + mp.getTitle()+"("+mp.getTimeNice()+")");
                    songs.add(mp.getMpcSong());
                }
                } catch (Exception exception) {
                exception.printStackTrace();
                }

            mpc.enqueSongs(songs);
            successful = true;
            Log.v("samba", "Successful" + successful);
        } catch (Exception e) {
            successful = false;
            e.printStackTrace();
        }
        return successful;
    }
    private ArrayList<String> readFileContent(SmbFile sFile) {
        StringBuilder builder=new StringBuilder();
        ArrayList<String> list=new ArrayList<String>();
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new InputStreamReader(new SmbFileInputStream(sFile)));
        } catch (SmbException ex) {
            //Logger.getLogger(SmbConnect.class.getName()).log(Level.SEVERE, null, ex);
        } catch (MalformedURLException ex) {
            //Logger.getLogger(SmbConnect.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnknownHostException ex) {
            //Logger.getLogger(SmbConnect.class.getName()).log(Level.SEVERE, null, ex);
        }
        String lineReader = null;
        {
            try {
                while ((lineReader = reader.readLine()) != null) {
                    list.add(lineReader);
                    builder.append(lineReader).append("\n");
                }
            } catch (IOException exception) {
                exception.printStackTrace();
            } finally {
                try {
                    reader.close();
                } catch (IOException ex) {
                    //Logger.getLogger(SmbConnect.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return list;
    }
}
